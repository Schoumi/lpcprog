/*
 * Copyright (C) 2016 Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.lpcprog.managers;

import android.content.Context;
import android.widget.ImageView;

import com.ftdi.j2xx.D2xxManager;
import com.ftdi.j2xx.FT_Device;

import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.util.Locale;

import fr.mobdev.lpcprog.R;
import fr.mobdev.lpcprog.listener.FlashListener;
import fr.mobdev.lpcprog.objects.Binary;
import fr.mobdev.lpcprog.objects.Part;
import fr.mobdev.lpcprog.objects.Server;
import fr.mobdev.lpcprog.objects.USBDevice;

/*
    Singleton that handle all ISP functions
*/
public class IspManager {

    //defines
    private static final int REP_BUF = 100;
    private static final int RETURN_CODE_SIZE = 3;
    private static final int SERIAL_BUF_SIZE = 1300;
    private static final int LINE_DATA_LENGTH = 45;
    private static final int LINES_PER_BLOCK = 20;
    private static final int MAX_DATA_BLOCK_SIZE = LINES_PER_BLOCK * LINE_DATA_LENGTH;
    private static final int UUENCODE_ADDED_VAL = 32;
    private static final int LINE_DATA_LENGTH_MAX = 45;

    //defines commands and some answers
    private static final byte[] SYNC_START = "?".getBytes();
    private static final byte[] SYNC = "Synchronized\r\n".getBytes();
    private static final byte[] SYNC_OK = "OK\r\n".getBytes();
    private static final byte[] DATA_BLOCK_OK = "OK\r\n".getBytes();
    private static final byte[] SYNC_ECHO_OFF = "A 0\r\n".getBytes();
    private static final byte[] READ_UID = "N\r\n".getBytes();
    private static final byte[] READ_PART_ID = "J\r\n".getBytes();
    private static final byte[] READ_BOOT_VERSION = "K\r\n".getBytes();
    private static final byte[] UNLOCK = "U 23130\r\n".getBytes();

    //members
    private static IspManager instance = null;
    private FT_Device serialPort = null;
    private Context context;
    private long part_id = 0;

    private IspManager(Context context) {
        this.context = context;
    }

    public static IspManager getInstance(Context context) {
        if (instance == null)
            instance = new IspManager(context);
        return instance;
    }

    public void setPartId(long part){
        part_id = part;
    }

    public boolean setupDevice(USBDevice device, int baudRate) {
        serialPort = device.device;
        if (serialPort != null)
            serialPort.resetDevice();

        if (serialPort != null && serialPort.isOpen()) {
            serialPort.setBaudRate(baudRate);
            byte dc1 = 0x11;
            byte dc3 = 0x13;
            serialPort.setFlowControl(D2xxManager.FT_FLOW_XON_XOFF, dc1, dc3);
            serialPort.setDataCharacteristics(D2xxManager.FT_DATA_BITS_8, D2xxManager.FT_STOP_BITS_1, D2xxManager.FT_PARITY_NONE);
            byte l = 0x2;
            serialPort.setLatencyTimer(l);
            return true;
        }
        return false;
    }

    public boolean synchronizeIfPossible(int crystal_freq) {
        if (!synchronize(crystal_freq)) {
            serialPort.purge(D2xxManager.FT_PURGE_TX);
            serialPort.purge(D2xxManager.FT_PURGE_RX);
            String[] uids = readUid();
            if (uids == null) {
                return false;
            }
        }

        return true;
    }

    private int clearBuffer(int size) {
        if (serialPort == null)
            return -1;

        byte[] buf = new byte[size];
        int ret = read(buf, size, 1000);
        return ret;
    }

    public String[] readUid() {
        if (serialPort == null)
            return null;
        int ret = sendCommand(READ_UID);

        if (ret != 0) {
            return null;
        }

        byte[] buf = new byte[REP_BUF];

        int size = read(buf, 50, 500);
        if (size <= 0) {
            return null;
        }
        String[] uids = new String[4];
        String buffer = new String(buf);

        if (buffer.contains("\r") && buffer.contains("\n"))
            buffer = buffer.replace("\n", "");
        else if (!buffer.contains("\r") && buffer.contains("\n"))
            buffer = buffer.replace("\n", "\r");

        for (int i = 0; i < 4; i++) {
            if (buffer.contains("\r")) {
                uids[i] = buffer.substring(0, buffer.indexOf("\r"));
                buffer = buffer.substring(buffer.indexOf("\r") + 1);
            }
        }
        return uids;
    }

    public String readPartId() {
        if (serialPort == null)
            return null;
        int ret = sendCommand(READ_PART_ID);
        if (ret != 0)
            return null;
        byte[] buf = new byte[REP_BUF];
        int size = read(buf, 20, 500);
        if (size <= 0)
            return null;
        String partId = new String(buf);
        partId = partId.substring(0, partId.indexOf("\r\n"));
        if (partId.contains("\r"))
            partId = partId.replace("\r", "");
        if (partId.contains("\n"))
            partId = partId.replace("\n", "");
        return partId;
    }

    public String[] readBootVersion() {
        if (serialPort == null)
            return null;
        int ret = sendCommand(READ_BOOT_VERSION);
        if (ret != 0) {
            return null;
        }
        byte[] buf = new byte[REP_BUF];

        int size = read(buf, 50, 500);

        if (size <= 0) {
            return null;
        }
        String[] version = new String[2];
        String buffer = new String(buf);

        if (buffer.contains("\r") && buffer.contains("\n"))
            buffer = buffer.replace("\n", "");
        else if (!buffer.contains("\r") && buffer.contains("\n"))
            buffer = buffer.replace("\n", "\r");

        for (int i = 0; i < 2; i++) {
            if (buffer.contains("\r")) {
                version[i] = buffer.substring(0, buffer.indexOf("\r"));
                buffer = buffer.substring(buffer.indexOf("\r") + 1);
            }
        }


        return version;
    }

    public int flashBinary(Server server, Binary binary, FlashListener listener) {
        Part parts = DatabaseManager.getInstance(context).getPart(part_id);
        listener.onStartFlash();
        if(parts == null) {
            listener.onError(context.getString(R.string.parts_missing));
            return -1;
        }
        int sector_size = parts.flash_size / parts.flash_nb_sectors;
        long ram_addr = parts.ram_base_addr + parts.ram_buffer_offset;
        boolean uuencode = parts.uuencode;
        byte data[];
        listener.onProgress(1,6,100,R.string.sanity_check);
        //Sanity Checks
        if (ram_addr > parts.ram_base_addr + parts.ram_size) {
            listener.onError(context.getString(R.string.buffer_out_ram));
            return -2;
        }

        long write_size = calcWriteSize(sector_size, parts.ram_buffer_size);
        if (write_size == 0) {
            listener.onError(context.getString(R.string.block_size_null));
            return -3;
        }

        listener.onProgress(2,6,0,R.string.erase_flash);
        int ret = eraseFlash(parts,listener);
        if (ret != 0) {
            listener.onError(context.getString(R.string.cant_erase_flash));
            return -4;
        }


        listener.onProgress(3,6,0,R.string.read_file);
        data = new byte[parts.flash_size];
        //copy file buffer to data array
        File file = new File(context.getApplicationInfo().dataDir + "/" + server.id + "/" + binary.path + binary.filename);
        int size = copy_buffer(file, data, parts.flash_size);
        if (size <= 0) {
            listener.onError(context.getString(R.string.copy_file));
            return -5;
        }
        listener.onProgress(3,6,100,R.string.read_file);

        fill_data_with_0(data, size, parts.flash_size);
        int cksum = calc_binary_checksum(data);
        setChecksum(data, 28, cksum);
        listener.onProgress(4,6,0,R.string.verify_checksum);
        if(!verifyChecksum(data,cksum)) {
            listener.onError(context.getString(R.string.checksum_failed));
            return -6;
        }
        listener.onProgress(4,6,100,R.string.verify_checksum);
        long blocks = (size / write_size) + ((size % write_size == 0) ? 0 : 1);
        if (blocks * write_size > parts.flash_size) {
            listener.onError(context.getString(R.string.flash_outside_end));
        }

        listener.onProgress(5,6,0,R.string.write_to_flash);
        for(int i = 0; i < blocks; i++){
            listener.onProgress(5,6,(int)(i*100/blocks),R.string.write_to_flash);
            int current_sector = (int)(i*write_size)/sector_size;
            int flash_addr = (int)(parts.flash_base_addr + (i*write_size));
            //prepare-for-write
            ret = send_cmd_sectors('P',current_sector,current_sector);
            if(ret != 0){
                listener.onError(context.getString(R.string.prepare_write_error)+" "+i);
                return ret;
            }

            ret = send_buf_to_ram(data,(int)(i*write_size),(int)ram_addr,(int)write_size, uuencode);
            if(ret != 0) {
                listener.onError(context.getString(R.string.write_ram_error)+" "+i);
                return ret;
            }

            //write-to-ram
            ret = send_cmd_address('C', flash_addr, (int)ram_addr, (int) write_size);
            if(ret != 0){
                listener.onError(context.getString(R.string.copy_to_flash_error)+" "+i);
                return ret;
            }

        }
        listener.onProgress(6,6,100,R.string.end_flash);
        listener.onSuccess();

        return ret;
    }

    /**** ISP Commands *****/

    private boolean synchronize(int crystal_freq) {
        if (serialPort == null)
            return false;
        int cpt = 0;
        while(clearBuffer(1000) > 0 && cpt < 20)
        {
            cpt++;
        }
        serialPort.purge(D2xxManager.FT_PURGE_RX);
        serialPort.purge(D2xxManager.FT_PURGE_TX);
        if(cpt == 20) {
            return false;
        }
        byte[] buf;
        if (serialPort.write(SYNC_START) != SYNC_START.length) {
            return false;
        }

        buf = new byte[SYNC.length];
        serialPort.read(buf, buf.length, 500);
        String result = new String(buf);
        String expected = new String(SYNC);
        if (result.compareTo(expected) != 0) {
            serialPort.write("\r\n".getBytes());
            return false;
        }
        if (serialPort.write(SYNC) != SYNC.length) {
            return false;
        }
        clearBuffer(SYNC.length);
        buf = new byte[SYNC_OK.length];
        serialPort.read(buf, buf.length, 500);
        result = new String(buf);
        expected = new String(SYNC_OK);
        if (result.compareTo(expected) != 0) {
            return false;
        }

        String freq = String.valueOf(crystal_freq) + "\r\n";
        if (serialPort.write(freq.getBytes()) != freq.length()) {
            return false;
        }

        clearBuffer(freq.length());
        buf = new byte[SYNC_OK.length];
        serialPort.read(buf, buf.length, 500);
        result = new String(buf);
        expected = new String(SYNC_OK);
        if (result.compareTo(expected) != 0) {
            return false;
        }

        if (serialPort.write(SYNC_ECHO_OFF) != SYNC_ECHO_OFF.length) {
            return false;
        }

        clearBuffer(SYNC_ECHO_OFF.length);
        buf = new byte[RETURN_CODE_SIZE];
        serialPort.read(buf, buf.length, 500);
        clearBuffer(REP_BUF);
        return true;


    }

    private int sendCommand(byte[] cmd) {
        byte[] buf = new byte[RETURN_CODE_SIZE];
        if (serialPort.write(cmd) != cmd.length)
            return -5;
        int len = read(buf, buf.length, 500);
        if (len <= 0)
            return -4;
        int ret = parseRetCode(buf);
        if(ret > 9)
            read(buf,1,50);
        return ret;
    }

    private int sendCommand(char c, int addr, int count) {
        int ret;
        int len;

        byte[] buf = String.format(Locale.getDefault(),"%c %d %d\r\n",c,addr,count).getBytes();
        len = buf.length;
        if(len > REP_BUF) {
            len = REP_BUF;
        }
        int sent = serialPort.write(buf,len);
        if(len != sent) {
            return -5;
        }
        buf = new byte[RETURN_CODE_SIZE];
        len = read(buf,buf.length,500);
        if(len <= 0) {
            return  -4;
        }
        ret = parseRetCode(buf);

        return ret;
    }

    private int send_cmd_sectors(char cmd, int first_sector, int last_sector) {
        byte[] buf = new byte[RETURN_CODE_SIZE];
        int ret;
        int len;

        if(last_sector < first_sector){
            return -6;
        }

        String request = String.format(Locale.getDefault(),"%c %d %d\r\n",cmd, first_sector, last_sector);
        len = serialPort.write(request.getBytes());
        if(len != request.getBytes().length){
            return -5;
        }
        len = read(buf,buf.length,500);
        if(len <= 0){
            return -4;
        }

        ret = parseRetCode(buf);

        return ret;
    }

    private int send_cmd_address(char c, int flash_addr, int ram_addr, int write_size) {
        int ret;
        int len;
        byte[] buf = new byte[RETURN_CODE_SIZE];
        String request = String.format(Locale.getDefault(),"%c %d %d %d\r\n",c,flash_addr,ram_addr,write_size);
        len = serialPort.write(request.getBytes());
        if(len != request.getBytes().length){
            return -5;
        }
        len = read(buf,buf.length,500);
        if (len <= 0) {
            return -4;
        }
        ret = parseRetCode(buf);

        return ret;
    }

    private int send_buf_to_ram(byte[] data, int offset, int ram_addr, int write_size, boolean uuencode) {
        int ret;
        int len;
        int blocks;
        int total_sent =0;

        byte[] dataToSend = new byte[write_size];
        System.arraycopy(data,offset,dataToSend,0,write_size);

        ret = sendCommand('W',ram_addr,write_size);
        if(ret != 0){
            return -8;
        }
        if(!uuencode) {
            len = serialPort.write(dataToSend);
            if(len != write_size){
                return -7;
            }
            return 0;
        }

        blocks = get_nb_blocks(write_size);

        int resend_requested_for_block = 0;
        for(int i = 0; i < blocks; i++){
            byte[] buf = new byte[SERIAL_BUF_SIZE];
            byte[] repbuf = new byte[REP_BUF];
            int data_size;
            int encoded_size;
            int computed_checksum;
            data_size = (write_size - total_sent);
            if( data_size >= MAX_DATA_BLOCK_SIZE)
                data_size = MAX_DATA_BLOCK_SIZE;
            encoded_size = uu_encode(buf,dataToSend,total_sent,data_size);
            computed_checksum = calc_checksum(dataToSend,total_sent,data_size);

            byte[] checksum = String.format(Locale.getDefault(),"%d\r\n", computed_checksum).getBytes();
            System.arraycopy(checksum,0,buf,encoded_size,checksum.length);
            encoded_size+=checksum.length;
            //buf[encoded_size]='\0';
            //encoded_size++;
            len = serialPort.write(buf,encoded_size);
            if(len != encoded_size) {
                return -8;
            }
            len = read(repbuf,REP_BUF,500);
            if(len <= 0) {
                return -9;
            }
            repbuf[len]='\0';
            String result = new String(repbuf,0,len);
            String expected = new String(DATA_BLOCK_OK);
            if(result.compareTo(expected)==0){
                total_sent += data_size;
                resend_requested_for_block = 0;
            } else {
                resend_requested_for_block++;
                if(resend_requested_for_block >= 3) {
                   return -10;
                }
                i--;
            }
        }

        return ret;
    }

    private int parseRetCode(byte[] data) {
        String str = new String(data,0,2);
        str.replace(" ","");
        str = str.replace("\r", "");
        str = str.replace("\n", "");
        if (!str.matches("[0-9 -+]*")||str.isEmpty())
            return -1;
        return Integer.parseInt(str);
    }

    private int eraseFlash(Part parts,FlashListener listener) {
        int ret = unlock();
        if (ret != 0) {
            return -1;
        }
        for (int i = 0; i < parts.flash_nb_sectors; i++) {
            listener.onProgress(2,5,i*100/parts.flash_nb_sectors,R.string.erase_flash);
            //blank-check
            ret = send_cmd_sectors('I', i, i);
            if (ret == 0) {
                continue;
            }

            if (ret < 0) {
                return ret;
            } else {
                byte buf[] = new byte[REP_BUF];
                read(buf, 40, 500);
            }
            //prepare-for-write
            ret = send_cmd_sectors('P', i, i);
            if (ret != 0) {
                return ret;
            }
            //erase
            ret = send_cmd_sectors('E', i, i);
            if (ret != 0) {
                return ret;
            }
        }
        listener.onProgress(2,5,100,R.string.erase_flash);
        return 0;
    }

    private int unlock() {
        int ret = sendCommand(UNLOCK);
        if(ret != 0)
            return -1;
        return 0;
    }

    /**** Miscellaneous Functions ****/

    private int uu_encode(byte[] data_out, byte[] data, int offset, int orig_size) {
        int new_size = 0;
        int pos = offset;
        while(pos < offset+orig_size) {
            int line_length = offset + orig_size - pos;
            int i = 0;
            if(line_length > LINE_DATA_LENGTH_MAX) {
                line_length = LINE_DATA_LENGTH_MAX;
            }
            data_out[new_size] = (byte) (line_length + UUENCODE_ADDED_VAL);
            new_size++;
            while (i < line_length) {
                int triplet = 0;
                for(int j = 0; j < 3; j++) {
                    triplet |= ((data[pos+i+j] & 0xFF) << (8 * (2-j)));
                    if(i+j >= line_length-1)
                        break;
                }
                for(int j = 0; j < 4; j++) {
                    data_out[new_size+j] = (byte)((triplet >> (6 * (3 - j))) & 0x3F);
                    data_out[new_size+j] += UUENCODE_ADDED_VAL;
                }
                i+=3;
                new_size += 4;
            }
            pos += line_length;
            data_out[new_size++] = '\r';
            data_out[new_size++] = '\n';
        }
        return new_size;
    }

    private int get_nb_blocks(int count) {
        int lines;
        int blocks;
        lines = count / LINE_DATA_LENGTH;
        if(count % LINE_DATA_LENGTH != 0) {
            lines += 1;
        }
        blocks = lines / LINES_PER_BLOCK;
        if(lines % LINES_PER_BLOCK != 0) {
            blocks += 1;
        }
        return blocks;
    }

    private boolean verifyChecksum(byte[] data, int cksum) {
        int verif = byteArrayToInt(data,28);
        return verif == cksum;
    }

    private void setChecksum(byte[] data, int offset, int cksum) {
        byte[] check = intToByteArray(cksum);
        System.arraycopy(check,0,data,offset,4);
    }

    private int calc_checksum(byte[] data, int offset, int datasize) {
        int sum = 0;
        for(int i = offset; i < offset+datasize; i++) {
            int b = 0;
            b |= (data[i] & 0xFF);
            sum = sum + b;
        }
        return sum;
    }

    private int calc_binary_checksum(byte[] data){
        int[] val = new int[7];
        val[0] = byteArrayToInt(data, 0);
        val[1] = byteArrayToInt(data, 4);
        val[2] = byteArrayToInt(data, 8);
        val[3] = byteArrayToInt(data, 12);
        val[4] = byteArrayToInt(data, 16);
        val[5] = byteArrayToInt(data, 20);
        val[6] = byteArrayToInt(data, 24);

        return 0 - val[0] - val[1] - val[2] - val[3] - val[4] - val[5] - val[6];
    }

    private void fill_data_with_0(byte[] data, int offset, int flash_size) {
        for (int i = offset; i < flash_size; i++)
            data[i] = 0;
    }

    private int copy_buffer(File file, byte[] data, int max_size) {
        int sizeReaded;
        int moreSize = 0;
        try {
            FileInputStream stream = new FileInputStream(file);
            int count = stream.read(data, 0, max_size);
            sizeReaded = count;
            if (count == max_size) {
                byte[] b = new byte[1];
                while (count > 0) {
                    count = stream.read(b, 0, 1);
                    if (count > 0)
                        moreSize += count;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
        if (sizeReaded == max_size && moreSize != 0)
            return -moreSize;
        return sizeReaded;
    }

    private long calcWriteSize(int sector_size, int ram_buffer_size) {
        long write_size = (sector_size < ram_buffer_size) ? sector_size : ram_buffer_size;
        if (write_size >= 4096) {
            write_size = 4096;
        } else if (write_size >= 1024) {
            write_size = 1024;
        } else if (write_size >= 512) {
            write_size = 512;
        } else if (write_size >= 256) {
            write_size = 256;
        } else if (write_size >= 64) {
            write_size = 64;
        } else {
            write_size = 0;
        }
        return write_size;
    }

    private int read(byte[] buffer, int size, int timeout) {
        long startTime = System.nanoTime();
        long timeoutNano = timeout * 1000000;
        int sizeRead = 0;

        ByteBuffer output = ByteBuffer.wrap(buffer, 0, size);

        while (sizeRead < size) {
            int sizeToRead = serialPort.getQueueStatus();
            if (sizeRead + sizeToRead > size)
                sizeToRead = size - sizeRead;
            if (sizeToRead > 0) {
                byte[] buf = new byte[sizeToRead];
                int sizeReceived = serialPort.read(buf, sizeToRead, 50);
                if (sizeReceived < 0)
                    return sizeReceived;
                sizeRead += sizeReceived;
                if (sizeReceived != 0)
                    output.put(buf, 0, sizeReceived);
            }
            long time = System.nanoTime();
            if (time - startTime >= timeoutNano)
                break;
        }
        return sizeRead;
    }

    private int byteArrayToInt(byte[] array, int offset) {
        int value = 0;
        value |= (array[offset] & 0xff);
        value |= (array[offset + 1] & 0xff) << 8;
        value |= (array[offset + 2] & 0xff) << 16;
        value |= (array[offset + 3] & 0xff) << 24;
        return value;
    }

    private byte[] intToByteArray(int value){
        byte[] output = new byte[4];
        output[0] = (byte)(value & 0xFF);
        output[1] = (byte)((value >> 8) & 0xFF);
        output[2] = (byte)((value >> 16) & 0xFF);
        output[3] = (byte)((value >> 24) & 0xFF);
        return output;
    }
}

