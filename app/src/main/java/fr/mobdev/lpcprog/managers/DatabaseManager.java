/*
 * Copyright (C) 2016  Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.lpcprog.managers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import fr.mobdev.lpcprog.objects.Binary;
import fr.mobdev.lpcprog.objects.Part;
import fr.mobdev.lpcprog.objects.Server;

/*
    Singleton use to manage all database access
*/
public class DatabaseManager extends SQLiteOpenHelper {

    private static DatabaseManager instance;

    public static DatabaseManager getInstance(Context context){
        if(instance == null)
            instance = new DatabaseManager(context);
        return instance;
    }

    private DatabaseManager(Context context){
        super(context,"lpcprog.db",null,1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("Create table if not exists binaries (" +
                   "id integer primary key autoincrement, server_id INTEGER, name varchar(1024), filename varchar(1024), version INTEGER, path varchar(1024), sha1 varchar(1024));");
        db.execSQL("Create table if not exists servers (" +
                   "id integer primary key autoincrement, url varchar(1024), isActive INTEGER, attempted INTEGER);");
        db.execSQL("Create table if not exists definitions (" +
                   "id integer primary key autoincrement, part_id INTEGER, part_name varchar(1024), fl_base_addr INTEGER, fl_size INTEGER, fl_nb_sect INTEGER," +
                   "reset_vector_offset INTEGER, ram_base_addr INTEGER, ram_size INTEGER, ram_buf_off INTEGER, ram_buf_size INTEGER, UU_encode INTEGER);");
        ContentValues values = new ContentValues();
        values.put("url","http://techdata.techno-innov.fr/Updates/");
        values.put("isActive",1);
        values.put("attempted",0);
        db.insert("servers",null,values);

        values.clear();
        values.put("part_id",0x00008100);
        values.put("part_name","LPC810M021FN8");
        values.put("fl_base_addr",0x0);
        values.put("fl_size",0x1000);
        values.put("fl_nb_sect",4);
        values.put("reset_vector_offset",0x04);
        values.put("ram_base_addr",0x10000000);
        values.put("ram_size",0x0400);
        values.put("ram_buf_off",0x300);
        values.put("ram_buf_size",0x100);
        values.put("UU_encode",false);
        db.insert("definitions",null,values);


        values.clear();
        values.put("part_id",0x00008122);
        values.put("part_name","LPC812M101JHD20");
        values.put("fl_base_addr",0x0);
        values.put("fl_size",0x4000);
        values.put("fl_nb_sect",16);
        values.put("reset_vector_offset",0x04);
        values.put("ram_base_addr",0x10000000);
        values.put("ram_size",0x1000);
        values.put("ram_buf_off",0x800);
        values.put("ram_buf_size",0x400);
        values.put("UU_encode",false);
        db.insert("definitions",null,values);

        values.clear();
        values.put("part_id",0x2540102B);
        values.put("part_name","LPC1114FHN33/302");
        values.put("fl_base_addr",0x0);
        values.put("fl_size",0x8000);
        values.put("fl_nb_sect",8);
        values.put("reset_vector_offset",0x04);
        values.put("ram_base_addr",0x10000000);
        values.put("ram_size",0x2000);
        values.put("ram_buf_off",0x800);
        values.put("ram_buf_size",0x400);
        values.put("UU_encode",true);
        db.insert("definitions",null,values);

        values.clear();
        values.put("part_id",0x3640C02B);
        values.put("part_name","LPC1224FBD48/101");
        values.put("fl_base_addr",0x0);
        values.put("fl_size",0x8000);
        values.put("fl_nb_sect",8);
        values.put("reset_vector_offset",0x04);
        values.put("ram_base_addr",0x10000000);
        values.put("ram_size",0x1000);
        values.put("ram_buf_off",0x800);
        values.put("ram_buf_size",0x400);
        values.put("UU_encode",true);
        db.insert("definitions",null,values);

        values.clear();
        values.put("part_id",0x26011922);
        values.put("part_name","LPC1764FBD100");
        values.put("fl_base_addr",0x0);
        values.put("fl_size",0x10000);
        values.put("fl_nb_sect",16);
        values.put("reset_vector_offset",0x04);
        values.put("ram_base_addr",0x10000000);
        values.put("ram_size",0x4000);
        values.put("ram_buf_off",0x800);
        values.put("ram_buf_size",0x800);
        values.put("UU_encode",true);
        db.insert("definitions",null,values);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Nothing to do right now
    }

    public void addServer(String url) {
        ContentValues values = new ContentValues();
        values.put("url",url);
        values.put("isActive",1);
        values.put("attempted",0);
        getWritableDatabase().insert("servers",null,values);
    }

    public void changeServerStatus(long id, boolean newStatus) {
        ContentValues values = new ContentValues();
        values.put("isActive",newStatus);
        String where = "id = ?";
        String whereArgs[] = new String[1];
        whereArgs[0] = String.valueOf(id);
        getWritableDatabase().update("servers",values,where,whereArgs);
    }

    public void deleteServer(long id)
    {
        String where = "id = ?";
        String whereArgs[] = new String[1];
        whereArgs[0] = String.valueOf(id);
        getWritableDatabase().delete("servers",where,whereArgs);
    }

    public List<List<Server>> getServers(Context context) {
        List<List<Server>> list = new ArrayList<>();
        List<Server> active = new ArrayList<>();
        List<Server> inactive = new ArrayList<>();
        List<Server> failed = new ArrayList<>();
        list.add(active);
        list.add(failed);
        list.add(inactive);
        Cursor cursor = getReadableDatabase().query("servers", null, null, null, null, null, null);
        while (cursor.moveToNext()) {
            int col = 0;
            try {
                Server server = new Server();
                server.id = cursor.getLong(col++);
                server.url = new URL(cursor.getString(col++));
                int isActive = cursor.getInt(col++);
                server.isActive = isActive != 0;
                server.attempted = cursor.getInt(col);
                server.binaries = getBinaries(context, server.id);
                if (server.isActive && server.attempted == 0)
                    active.add(server);
                else if (server.isActive && server.attempted != 0)
                    failed.add(server);
                else
                    inactive.add(server);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        cursor.close();
        return list;
    }

    private List<Binary> getBinaries(Context context, long id) {
        List<Binary> binaries = new ArrayList<>();
        String whereClause = "server_id = ?";
        String[] whereArgs = new String[1];
        whereArgs[0] = String.valueOf(id);
        Cursor cursor = getReadableDatabase().query("binaries", null, whereClause, whereArgs, null, null, null);
        while (cursor.moveToNext()){
            int col = 0;
            Binary b = new Binary();
            b.id = cursor.getLong(col++);
            col++;//ignore server id
            b.name = cursor.getString(col++);
            b.filename = cursor.getString(col++);
            b.version = cursor.getInt(col++);
            b.path = cursor.getString(col++);
            b.sha1 = cursor.getString(col);
            File file = new File(context.getApplicationInfo().dataDir+"/"+id+"/"+b.path+b.filename);
            b.isDownloaded = file.exists();
            b.progress = 0;
            b.isDownloading = false;
            binaries.add(b);
        }
        cursor.close();
        return binaries;
    }

    void serverFail(Server server) {
        server.attempted++;
        if(server.attempted == 5){
            server.attempted = 0;
            server.isActive = false;
        }
        ContentValues values = new ContentValues();
        values.put("attempted",server.attempted);
        values.put("isActive",server.isActive);
        String whereClause = "id = ?";
        String[] whereArgs = new String[1];
        whereArgs[0] = String.valueOf(server.id);
        getWritableDatabase().update("servers",values,whereClause,whereArgs);
    }

    void addBinary(Server server, Binary binary) {
        ContentValues values = new ContentValues();
        values.put("server_id",server.id);
        values.put("name",binary.name);
        values.put("filename",binary.filename);
        values.put("version",binary.version);
        values.put("path",binary.path);
        values.put("sha1",binary.sha1);
        values.put("name",binary.name);
        getWritableDatabase().insert("binaries",null,values);
    }

    public void deleteBinary(long id) {
        String whereClause = "id = ?";
        String[] whereArgs = new String[1];
        whereArgs[0] = String.valueOf(id);
        getWritableDatabase().delete("binaries",whereClause,whereArgs);
    }

    public Part getPart(long part_id) {
        Part part = null;
        String whereClause = "part_id = ?";
        String[] whereArgs = new String[1];
        whereArgs[0] = String.valueOf(part_id);
        Cursor cursor = getReadableDatabase().query("definitions", null, whereClause, whereArgs, null, null, null);
        if (cursor.moveToNext()) {
            part = new Part();
            int col = 0;
            part.id = cursor.getLong(col++);
            part.part_id = cursor.getInt(col++);
            part.part_name = cursor.getString(col++);
            part.flash_base_addr = cursor.getInt(col++);
            part.flash_size = cursor.getInt(col++);
            part.flash_nb_sectors = cursor.getInt(col++);
            part.reset_vector_offset = cursor.getInt(col++);
            part.ram_base_addr = cursor.getInt(col++);
            part.ram_size = cursor.getInt(col++);
            part.ram_buffer_offset = cursor.getInt(col++);
            part.ram_buffer_size = cursor.getInt(col++);
            part.uuencode = cursor.getInt(col) == 1;
        }
        cursor.close();

        return part;
    }

    public List<Part> getParts() {
        List<Part> parts = new ArrayList<>();
        String[] columns = new String[1];
        columns[0] = "part_id";
        Cursor cursor = getReadableDatabase().query("definitions",columns,null,null,null,null,null);
        while (cursor.moveToNext()) {
            long id = cursor.getLong(0);
            Part part = getPart(id);
            parts.add(part);
        }
        cursor.close();
        return parts;
    }

    public void deletePart(long id) {
        String whereClause = "id = ?";
        String whereArgs[] = new String[1];
        whereArgs[0] = String.valueOf(id);
        getWritableDatabase().delete("definitions",whereClause,whereArgs);
    }

    public void addPart(Part part) {
        ContentValues values = new ContentValues();
        values.put("part_id",part.part_id);
        values.put("part_name",part.part_name);
        values.put("fl_base_addr",part.flash_base_addr);
        values.put("fl_size",part.flash_size);
        values.put("fl_nb_sect",part.flash_nb_sectors);
        values.put("reset_vector_offset",part.reset_vector_offset);
        values.put("ram_base_addr",part.ram_base_addr);
        values.put("ram_size",part.ram_size);
        values.put("ram_buf_off",part.ram_buffer_offset);
        values.put("ram_buf_size",part.ram_buffer_size);
        values.put("UU_encode",part.uuencode);
        getWritableDatabase().insert("definitions",null,values);

    }

    public void updatePart(Part part) {
        String whereClause = "id = ?";
        String whereArgs[] = new String[1];
        whereArgs[0] = String.valueOf(part.id);
        ContentValues values = new ContentValues();
        values.put("part_id",part.part_id);
        values.put("part_name",part.part_name);
        values.put("fl_base_addr",part.flash_base_addr);
        values.put("fl_size",part.flash_size);
        values.put("fl_nb_sect",part.flash_nb_sectors);
        values.put("reset_vector_offset",part.reset_vector_offset);
        values.put("ram_base_addr",part.ram_base_addr);
        values.put("ram_size",part.ram_size);
        values.put("ram_buf_off",part.ram_buffer_offset);
        values.put("ram_buf_size",part.ram_buffer_size);
        values.put("UU_encode",part.uuencode);
        getWritableDatabase().update("definitions",values,whereClause,whereArgs);
    }
}
