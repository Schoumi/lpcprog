/*
 * Copyright (C) 2016 Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.lpcprog.managers;

import android.content.Context;

import com.ftdi.j2xx.D2xxManager;

import java.util.ArrayList;
import java.util.List;

import fr.mobdev.lpcprog.objects.USBDevice;
/*
    Manager use to manage usb devices
*/
public class UsbCommManager {

    private static UsbCommManager instance = null;

    private D2xxManager manager = null;
    private Context context;
    private List<USBDevice> openedDevices;


    private UsbCommManager(Context context){
        openedDevices = new ArrayList<>();
        try {
            manager = D2xxManager.getInstance(context);
            this.context = context;
        } catch (D2xxManager.D2xxException e) {
            e.printStackTrace();
        }
    }

    public static UsbCommManager getInstance(Context context){
        if(instance == null)
            instance = new UsbCommManager(context);
        return instance;
    }

    public List<USBDevice> getDevices(){
        if(manager == null)
            return null;

        List<USBDevice> devicesList = new ArrayList<>();
        int deviceCount = manager.createDeviceInfoList(context);
        if(deviceCount > 0) {
            D2xxManager.FtDeviceInfoListNode[] devices = new D2xxManager.FtDeviceInfoListNode[deviceCount];
            manager.getDeviceInfoList(deviceCount, devices);
            for (int i = 0; i < deviceCount; i++) {
                int deviceVID = devices[i].id >> 16;
                int devicePID = devices[i].id & 0xFFFF;
                if (deviceVID != 0x1d6b && devicePID != 0x0001 && devicePID != 0x0002 && devicePID != 0x0003) {
                    USBDevice device = new USBDevice();
                    device.VID = deviceVID;
                    device.PID = devicePID;
                    device.description = devices[i].description;
                    devicesList.add(device);
                }
            }
        }
        return devicesList;
    }

    public boolean openConnection(USBDevice device) {
        if(manager == null)
            return false;
        int deviceCount = manager.createDeviceInfoList(context);
        if (deviceCount > 0) {
            D2xxManager.FtDeviceInfoListNode[] devices = new D2xxManager.FtDeviceInfoListNode[deviceCount];
            manager.getDeviceInfoList(deviceCount,devices);
            for (USBDevice dev : openedDevices) {
                if(dev.PID == device.PID && dev.VID == device.VID && dev.device.isOpen()){
                    device.device = dev.device;
                    return true;
                }
                else if(dev.PID == device.PID && dev.VID == device.VID && !dev.device.isOpen()){
                    openedDevices.remove(dev);
                }
            }
            for (int i = 0; i < deviceCount; i++) {
                int deviceVID = devices[i].id >> 16;
                int devicePID = devices[i].id & 0xFFFF;
                if (deviceVID == device.VID && devicePID == device.PID) {
                    device.device = manager.openByIndex(context,i);
                    if(device.device != null)
                        openedDevices.add(device);
                    return true;
                }
            }
        }
        return false;
    }
}

