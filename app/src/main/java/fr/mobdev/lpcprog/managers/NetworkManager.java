/*
 * Copyright (C) 2016 Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.lpcprog.managers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

import fr.mobdev.lpcprog.R;
import fr.mobdev.lpcprog.listener.DownloadListener;
import fr.mobdev.lpcprog.listener.NetworkListener;
import fr.mobdev.lpcprog.objects.Binary;
import fr.mobdev.lpcprog.objects.Server;

/*
    Singleton that handle all network connection
*/
public class NetworkManager {

    private NetworkListener listener;
    private static NetworkManager instance;

    private NetworkManager(NetworkListener listener)
    {
        this.listener = listener;
    }

    public static NetworkManager getInstance(NetworkListener listener){
        if (instance == null)
            instance = new NetworkManager(listener);
        if(listener != null && listener != instance.getListener())
            instance.setListener(listener);
        return instance;
    }

    private NetworkListener getListener()
    {
        return listener;
    }

    private void setListener(NetworkListener listener)
    {
        this.listener = listener;
    }

    public void browseBinaries(final Context context){
        if(isConnectedToInternet(context)) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    getBinariesList(context);
                }
            }).start();
        }
        else
            listener.onError(context.getString(R.string.network_error));
    }

    public void retrieveBinary(final Context context, final Server server, final Binary binary, final DownloadListener downloadListener){
        if(isConnectedToInternet(context)){
           new Thread(new Runnable() {
               @Override
               public void run() {
                   getBinary(context,server, binary,downloadListener);
               }
           }).start();
        }
        else
            downloadListener.downloadError(server,binary,context.getString(R.string.network_error));
    }

    private void getBinariesList(Context context){
        List<List<Server>> servers = DatabaseManager.getInstance(context).getServers(context);
        List<Server> serversAvailable = new ArrayList<>();
        for(Server server : servers.get(0)){
            serversAvailable.add(server);
        }
        for(Server server : servers.get(1)){
            serversAvailable.add(server);
        }

        listener.startBinaries();
        List<Server> serverInError = new ArrayList<>();
        for(Server server : serversAvailable) {
            listener.startServer(server);
            try {
                URL url = new URL(server.url.toString()+"Binaries");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                InputStream stream = connection.getInputStream();
                if(stream != null){
                    InputStreamReader isr = new InputStreamReader(stream);
                    BufferedReader reader = new BufferedReader(isr);
                    String line = reader.readLine();
                    while(line != null){
                        if(!line.startsWith("#")) {
                            Binary b = parseLine(line);
                            if (b != null && !server.contains(b)) {
                                server.addBinary(b);
                                DatabaseManager.getInstance(context).addBinary(server, b);
                            }
                        }
                        line = reader.readLine();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                DatabaseManager.getInstance(context).serverFail(server);
                serverInError.add(server);
            } finally {
             listener.endServer(server);
            }
        }
        if(serverInError.containsAll(servers))
            listener.onError(context.getString(R.string.all_server_error));
        listener.endBinaries();
    }

    private Binary parseLine(String line){
        if(!line.matches("(.*;){5}")){
            return null;
        }
        Binary b = new Binary();
        b.name = line.substring(0,line.indexOf(";"));
        line = line.substring(line.indexOf(";")+1);
        b.filename = line.substring(0,line.indexOf(";"));
        line = line.substring(line.indexOf(";")+1);
        b.version = Integer.valueOf(line.substring(0,line.indexOf(";")));
        line = line.substring(line.indexOf(";")+1);
        b.path = line.substring(0,line.indexOf(";"));
        line = line.substring(line.indexOf(";")+1);
        b.sha1 = line.substring(0,line.indexOf(";"));
        b.isDownloaded = false;
        b.isDownloading = false;
        b.progress = 0;
        return b;
    }

    private void getBinary(Context context, Server server, Binary binary, DownloadListener downloadListener){
        try {
            URL url = new URL(server.url.toString() + binary.path + binary.filename);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            InputStream stream = connection.getInputStream();
            File file = new File(context.getApplicationInfo().dataDir+"/"+server.id+"/"+binary.path+binary.filename);
            if ((!file.getParentFile().exists() && !file.getParentFile().mkdirs()) || !file.createNewFile() && !file.canWrite()){
                downloadListener.downloadError(server,binary,context.getString(R.string.cant_create_file));
                return;
            }
            FileOutputStream output = new FileOutputStream(file);
            int fileLength = connection.getContentLength();
            if (stream != null) {
                byte[] data = new byte[4096];
                long totalSizeReaded = 0;
                int sizeReaded ;
                while((sizeReaded = stream.read(data)) != -1){
                    totalSizeReaded += sizeReaded;
                    if(fileLength > 0){
                        downloadListener.progress(server,binary, (int) totalSizeReaded * 100 / fileLength);
                    }
                    output.write(data,0,sizeReaded);
                }
                output.close();
                if(checkFileSha1(context,server,binary))
                    downloadListener.downloadSuccessful(server,binary);
                else
                    downloadListener.downloadError(server,binary,context.getString(R.string.sha1_error));
            }
        }catch (IOException e) {
            e.printStackTrace();
            cleanupFile(context,server,binary);
            DatabaseManager.getInstance(context).serverFail(server);
            downloadListener.downloadError(server,binary, context.getString(R.string.unknown_error));
        }
    }

    private boolean checkFileSha1(Context context, Server server, Binary binary){
        String sha1 = "";
        try {
            File f = new File(context.getApplicationInfo().dataDir+"/"+server.id+"/"+binary.path+binary.filename);
            FileInputStream fis = new FileInputStream(f);
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            byte[] data = new byte[4096];
            int sizeReaded;
            while((sizeReaded = fis.read(data)) >0){
                md.update(data,0,sizeReaded);
            }
            byte[] output = md.digest();
            for(byte b : output){
                sha1 += String.format("%02x",b);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sha1.compareToIgnoreCase(binary.sha1) == 0;
    }

    private boolean cleanupFile(Context context, Server server, Binary binary){
        File f = new File(context.getApplicationInfo().dataDir+"/"+server.id+"/"+binary.path+binary.filename);
        return f.delete();
    }

    private boolean isConnectedToInternet(Context context)
    {
        //verify the connectivity
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null)
        {
            NetworkInfo.State networkState = networkInfo.getState();
            if (networkState.equals(NetworkInfo.State.CONNECTED))
            {
                return true;
            }
        }
        return false;
    }
}
