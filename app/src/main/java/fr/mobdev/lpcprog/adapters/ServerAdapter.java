/*
 * Copyright (C) 2016 Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.lpcprog.adapters;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import fr.mobdev.lpcprog.R;
import fr.mobdev.lpcprog.listener.ServerListener;
import fr.mobdev.lpcprog.managers.DatabaseManager;
import fr.mobdev.lpcprog.objects.Server;

/*
    Adapter use to display servers in different sections
*/

public class ServerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int SECTION = 0;
    private static final int FIELD = 1;

    private List<List<Server>> servers;
    private List<String> sections;
    private ServerListener serverListener;
    private int count;

    public ServerAdapter(List<List<Server>> servers, List<String> sections, ServerListener listener) {
        this.servers = servers;
        this.sections = sections;
        serverListener = listener;
        countElements();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == SECTION) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.server_section, parent, false);
            return new SectionHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.server_item, parent, false);
            return new ServerHolder(view,serverListener);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof SectionHolder) {
            SectionHolder sectionHolder = (SectionHolder) holder;
            int section = getSection(position);
            String sectionName = sections.get(section);
            sectionHolder.setupSection(sectionName);
        } else if (holder instanceof ServerHolder) {
            ServerHolder serverHolder = (ServerHolder) holder;

            int section = getSection(position);
            int pos = getRealPosition(position);
            Server server = servers.get(section).get(pos);
            serverHolder.setupServer(server);
        }
    }

    private void countElements() {
        int size = 0;
        for (int i = 0; i < sections.size(); i++) {
            if (servers.get(i).size() > 0) {
                size++;
                size += servers.get(i).size();
            }
        }
        count = size;
    }

    private int getRealPosition(int fakePosition) {
        fakePosition -= 1;
        for (int i = 0; i < sections.size(); i++) {
            int serverSize = servers.get(i).size();
            if (fakePosition - serverSize < 0)
                return fakePosition;
            if (serverSize > 0) {
                fakePosition -= 1;
                fakePosition -= serverSize;
            }
        }
        return -1;
    }

    private int getSection(int fakePosition) {
        int prev_size = 1;
        for (int i = 0; i < sections.size(); i++) {
            int serverSize = servers.get(i).size();
            if (fakePosition < prev_size + serverSize)
                return i;
            if (servers.get(i).size() > 0) {
                prev_size += 1;
                prev_size += serverSize;
            }
        }
        return -1;//error
    }

    @Override
    public int getItemCount() {
        return count;
    }

    @Override
    public int getItemViewType(int position) {
        int nbValues = 0;
        for (int i = 0; i < sections.size(); i++) {
            if (position == i + nbValues)
                return SECTION;
            else if (position < i + nbValues)
                break;
            nbValues += servers.get(i).size();
        }
        return FIELD;
    }
}

class ServerHolder extends RecyclerView.ViewHolder{
    private ServerListener serverListener;
    ServerHolder(View itemView, ServerListener serverListener) {
        super(itemView);
        this.serverListener = serverListener;
    }

    void setupServer(final Server server){
        TextView view = (TextView) itemView.findViewById(R.id.server_name);
        view.setText(server.url.toString());
        ImageView deleteView = (ImageView) itemView.findViewById(R.id.server_delete);
        deleteView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(itemView.getContext());
                builder.setMessage(itemView.getContext().getString(R.string.delete_server_message)+" "+server.url)
                        .setCancelable(false)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //delete server from database and update the view
                                DatabaseManager.getInstance(itemView.getContext()).deleteServer(server.id);
                                serverListener.updateServerList();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                final AlertDialog alert = builder.create();
                alert.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialogInterface) {
                        alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(0xFF000000);
                        alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(0xFF000000);
                    }
                });
                alert.show();
            }
        });
    }
}

class SectionHolder extends RecyclerView.ViewHolder {

    SectionHolder(View itemView) {
        super(itemView);
    }

    void setupSection(String sectionName) {
        TextView view = (TextView)itemView.findViewById(R.id.section_name);
        view.setText(sectionName);
    }
}

