/*
 * Copyright (C) 2016 Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.lpcprog.adapters;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.List;

import fr.mobdev.lpcprog.R;
import fr.mobdev.lpcprog.fragment.BrowseRepositoryFragment;
import fr.mobdev.lpcprog.managers.DatabaseManager;
import fr.mobdev.lpcprog.objects.Binary;
import fr.mobdev.lpcprog.objects.Server;
/*
    Adapter that handle Binaries List. Binaries are in sections depending on servers. There's only
    one list, the class handle sections and items in the adapter
*/
public class BinariesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int SECTION = 0;
    private static final int FIELD = 1;

    private List<Server> servers;
    private int count;
    private BrowseRepositoryFragment.BrowseRepositoryListener listener;

    public BinariesAdapter(List<Server> servers, BrowseRepositoryFragment.BrowseRepositoryListener listener) {
        this.servers = servers;
        this.listener = listener;
        countElements();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == SECTION) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.server_section, parent, false);
            return new SectionHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.binary_item, parent, false);
            final BinaryHolder viewHolder = new BinaryHolder(view);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onBinaryClick(viewHolder.server,viewHolder.binary);
                }
            });
            return viewHolder;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof SectionHolder) {
            SectionHolder sectionHolder = (SectionHolder) holder;
            int section = getSection(position);
            if(section == -1)
                return;
            String sectionName = servers.get(section).url.toString();
            sectionHolder.setupSection(sectionName);
        } else if (holder instanceof BinaryHolder) {
            BinaryHolder binaryHolder = (BinaryHolder) holder;

            int section = getSection(position);
            int pos = getRealPosition(position);
            if(section == -1 || pos == -1)
                return;
            Server server = servers.get(section);
            Binary binary = servers.get(section).binaries.get(pos);
            binaryHolder.setupBinary(server,binary);
        }
    }

    private void countElements() {
        int size = 0;
        for (int i = 0; i < servers.size(); i++) {
            if (servers.get(i).binaries.size() > 0) {
                size++;
                size += servers.get(i).binaries.size();
            }
        }
        count = size;
    }

    private int getRealPosition(int fakePosition) {
        fakePosition -= 1;
        for (int i = 0; i < servers.size(); i++) {
            int serverSize = servers.get(i).binaries.size();
            if (fakePosition - serverSize < 0)
                return fakePosition;
            if (serverSize > 0) {
                fakePosition -= 1;
                fakePosition -= serverSize;
            }
        }
        return -1;
    }

    private int getSection(int fakePosition) {
        int prev_size = 1;
        for (int i = 0; i < servers.size(); i++) {
            int serverSize = servers.get(i).binaries.size();
            if (fakePosition < prev_size + serverSize)
                return i;
            if (servers.get(i).binaries.size() > 0) {
                prev_size += 1;
                prev_size += serverSize;
            }
        }
        return -1;//error
    }

    @Override
    public int getItemCount() {
        return count;
    }

    @Override
    public int getItemViewType(int position) {
        int nbValues = 0;
        for (int i = 0; i < servers.size(); i++) {
            if (position == i + nbValues)
                return SECTION;
            else if (position < i + nbValues)
                break;
            nbValues += servers.get(i).binaries.size();
        }
        return FIELD;
    }
}

class BinaryHolder extends RecyclerView.ViewHolder{

    Binary binary;
    Server server;

    BinaryHolder(View itemView) {
        super(itemView);
    }

    @SuppressLint("SetTextI18n")
    void setupBinary(final Server server, final Binary binary){
        this.binary = binary;
        this.server = server;
        TextView name = itemView.findViewById(R.id.binary_name);
        TextView version = itemView.findViewById(R.id.binary_version);

        final ImageView deleteView = itemView.findViewById(R.id.delete_download);
        final ImageView downloadView = itemView.findViewById(R.id.download);

        name.setText(binary.name);
        //TODO Fix Internationalisation warning
        version.setText("Vers. "+binary.version);
        if(binary.isDownloaded) {
            downloadView.setVisibility(View.VISIBLE);
            deleteView.setVisibility(View.VISIBLE);
        } else {
            downloadView.setVisibility(View.GONE);
            deleteView.setVisibility(View.GONE);
        }

        deleteView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(itemView.getContext());
                builder.setMessage(itemView.getContext().getString(R.string.delete_binary_message)+" "+binary.name+" "+
                                   itemView.getContext().getString(R.string.version)+" "+binary.version+" ?")
                        .setCancelable(false)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //delete server from database and update the view
                                File file = new File(itemView.getContext().getApplicationInfo().dataDir+"/"+server.id+"/"+binary.path+binary.filename);
                                if(file.exists()) {
                                    //noinspection ResultOfMethodCallIgnored
                                    file.delete();
                                }
                                binary.isDownloaded = false;
                                DatabaseManager.getInstance(itemView.getContext()).deleteBinary(binary.id);
                                //file and reset all downloaded items
                                downloadView.setVisibility(View.GONE);
                                deleteView.setVisibility(View.GONE);
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                final AlertDialog alert = builder.create();
                alert.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialogInterface) {
                        alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(0xFF000000);
                        alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(0xFF000000);
                    }
                });
                alert.show();
            }
        });
    }
}

