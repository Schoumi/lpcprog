/*
 * Copyright (C) 2016 Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.lpcprog.adapters;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import fr.mobdev.lpcprog.R;
import fr.mobdev.lpcprog.fragment.PartsFragment;
import fr.mobdev.lpcprog.managers.DatabaseManager;
import fr.mobdev.lpcprog.objects.Part;

/*
    Adapter use to display list of µc definitions
*/
public class PartsAdapter extends RecyclerView.Adapter<PartHolder>{

    private List<Part> parts;
    private PartsFragment.OnEditPressListener listener;

    public PartsAdapter(List<Part> devices, PartsFragment.OnEditPressListener listener){
        this.parts = devices;
        this.listener = listener;
    }

    @Override
    public PartHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.part_item,parent,false);
        return new PartHolder(view,listener);
    }

    @Override
    public void onBindViewHolder(PartHolder holder, int position) {
        holder.setupPart(parts.get(position));
    }

    @Override
    public int getItemCount() {
        return parts.size();
    }
}

class PartHolder extends RecyclerView.ViewHolder {

    private PartsFragment.OnEditPressListener listener;

    PartHolder(View itemView, PartsFragment.OnEditPressListener listener) {
        super(itemView);
        this.listener = listener;
    }

    void setupPart(final Part part){
        TextView name = (TextView) itemView.findViewById(R.id.part_name);
        name.setText(part.part_name);
        ImageView deleteView = (ImageView) itemView.findViewById(R.id.part_delete);
        deleteView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(itemView.getContext());
                builder.setMessage(itemView.getContext().getString(R.string.delete_part_message)+" "+part.part_name)
                        .setCancelable(false)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //delete server from database and update the view
                                DatabaseManager.getInstance(itemView.getContext()).deletePart(part.id);
                                listener.onDeletePress();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                final AlertDialog alert = builder.create();
                alert.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialogInterface) {
                        alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(0xFF000000);
                        alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(0xFF000000);
                    }
                });
                alert.show();
            }
        });

        ImageView editView = (ImageView) itemView.findViewById(R.id.part_edit);
        editView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onEditPress(part.part_id);
            }
        });
    }
}
