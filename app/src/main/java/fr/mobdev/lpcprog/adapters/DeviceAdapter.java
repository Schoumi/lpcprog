/*
 * Copyright (C) 2016 Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.lpcprog.adapters;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import fr.mobdev.lpcprog.R;
import fr.mobdev.lpcprog.fragment.BrowseDeviceFragment;
import fr.mobdev.lpcprog.objects.USBDevice;

/*
    Adapter use to display devices pluggued on the device through OTG
*/
public class DeviceAdapter extends RecyclerView.Adapter<DeviceHolder> {

    BrowseDeviceFragment.BrowseDeviceListener listener;

    private List<USBDevice> devices;
    public DeviceAdapter(List<USBDevice> devices, BrowseDeviceFragment.BrowseDeviceListener listener){
        this.devices = devices;
        this.listener = listener;
    }

    @Override
    public DeviceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.device_item,parent,false);
        final DeviceHolder holder = new DeviceHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onDeviceClick(holder.usbDevice);
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(DeviceHolder holder, int position) {
        holder.setupDevice(devices.get(position));
    }

    @Override
    public int getItemCount() {
        return devices.size();
    }
}

class DeviceHolder extends RecyclerView.ViewHolder {

    USBDevice usbDevice;

    DeviceHolder(View itemView) {
        super(itemView);
    }

    @SuppressLint("SetTextI18n")
    void setupDevice(USBDevice device){
        usbDevice = device;
        TextView name = itemView.findViewById(R.id.device_name);
        name.setText(itemView.getContext().getString(R.string.dev_name) + device.description);
        TextView vendor = itemView.findViewById(R.id.device_vendor_id);
        vendor.setText(itemView.getContext().getString(R.string.vendor_id)+String.format("%04x",device.VID));
        TextView product = itemView.findViewById(R.id.device_id);
        product.setText(itemView.getContext().getString(R.string.product_id)+String.format("%04x", device.PID));

    }

}
