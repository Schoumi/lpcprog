/*
 * Copyright (C) 2016 Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.lpcprog.fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import fr.mobdev.lpcprog.R;
import fr.mobdev.lpcprog.managers.DatabaseManager;
import fr.mobdev.lpcprog.objects.Part;

/*
    Fragment that Handle add µC definitions to the Database
*/
public class AddPartFragment extends Fragment {

    private Part part;

    private OnAddInteractionListener listener;
    private List<TextWatcher> watchers;

    public AddPartFragment() {
        // Required empty public constructor
    }

    public static AddPartFragment newInstance(long part_id) {
        AddPartFragment fragment = new AddPartFragment();
        Bundle args = new Bundle();
        args.putLong("part_id", part_id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            long part_id = getArguments().getLong("part_id");
            if(part_id != -1)
                part = DatabaseManager.getInstance(getActivity()).getPart(part_id);
            else
                part = null;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        watchers = new ArrayList<>();
        final View v = inflater.inflate(R.layout.add_part, container, false);
        EditText t = (EditText) v.findViewById(R.id.edit_part_id);
        watchers.add(new HexaWatcher(t,this));
        t.addTextChangedListener(watchers.get(watchers.size()-1));
        t.setText(part != null ? "0x"+String.format("%08x",part.part_id).toUpperCase() : "0x");
        t = (EditText) v.findViewById(R.id.edit_flash_base_addr);
        watchers.add(new HexaWatcher(t,this));
        t.addTextChangedListener(watchers.get(watchers.size()-1));
        t.setText(part != null ? "0x"+String.format("%08x",part.flash_base_addr).toUpperCase() : "0x");
        t = (EditText) v.findViewById(R.id.edit_flash_size);
        watchers.add(new HexaWatcher(t,this));
        t.addTextChangedListener(watchers.get(watchers.size()-1));
        t.setText(part != null ? "0x"+String.format("%04x",part.flash_size).toUpperCase() : "0x");
        t = (EditText) v.findViewById(R.id.edit_reset_vector_offset);
        watchers.add(new HexaWatcher(t,this));
        t.addTextChangedListener(watchers.get(watchers.size()-1));
        t.setText(part != null ? "0x"+String.format("%02x",part.reset_vector_offset).toUpperCase() : "0x");
        t = (EditText) v.findViewById(R.id.edit_ram_base_addr);
        watchers.add(new HexaWatcher(t,this));
        t.addTextChangedListener(watchers.get(watchers.size()-1));
        t.setText(part != null ? "0x"+String.format("%08x",part.ram_base_addr).toUpperCase() : "0x");
        t = (EditText) v.findViewById(R.id.edit_ram_size);
        watchers.add(new HexaWatcher(t,this));
        t.addTextChangedListener(watchers.get(watchers.size()-1));
        t.setText(part != null ? "0x"+String.format("%04x",part.ram_size).toUpperCase() : "0x");
        t = (EditText) v.findViewById(R.id.edit_ram_buf_offset);
        watchers.add(new HexaWatcher(t,this));
        t.addTextChangedListener(watchers.get(watchers.size()-1));
        t.setText(part != null ? "0x"+String.format("%03x",part.ram_buffer_offset).toUpperCase() : "0x");
        t = (EditText) v.findViewById(R.id.edit_ram_buf_size);
        watchers.add(new HexaWatcher(t,this));
        t.addTextChangedListener(watchers.get(watchers.size()-1));
        t.setText(part != null ? "0x"+String.format("%03x",part.ram_buffer_size).toUpperCase() : "0x");

        t = (EditText) v.findViewById(R.id.edit_part_name);
        watchers.add(new EmptyWatcher(t,this));
        t.addTextChangedListener(watchers.get(watchers.size()-1));
        t.setText(part != null ? part.part_name : "");
        t = (EditText) v.findViewById(R.id.edit_flash_nb_sector);
        watchers.add(new EmptyWatcher(t,this));
        t.addTextChangedListener(watchers.get(watchers.size()-1));
        t.setText(part != null ? String.valueOf(part.flash_nb_sectors) : "");

        Spinner s = (Spinner) v.findViewById(R.id.spinner_uuencode);
        if(part != null)
            s.setSelection(part.uuencode ? 0 : 1);

        Button b = (Button) v.findViewById(R.id.submit);
        b.setTextColor(0xFF000000);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(part == null) {
                    part = new Part();
                    part.id = -1;
                }
                EditText t = (EditText) v.findViewById(R.id.edit_part_id);
                part.part_id = Long.parseLong(t.getText().toString().replace("0x",""),16);
                t = (EditText) v.findViewById(R.id.edit_flash_base_addr);
                part.flash_base_addr = Integer.parseInt(t.getText().toString().replace("0x",""),16);
                t = (EditText) v.findViewById(R.id.edit_flash_size);
                part.flash_size = Integer.parseInt(t.getText().toString().replace("0x",""),16);
                t = (EditText) v.findViewById(R.id.edit_reset_vector_offset);
                part.reset_vector_offset = Integer.parseInt(t.getText().toString().replace("0x",""),16);
                t = (EditText) v.findViewById(R.id.edit_ram_base_addr);
                part.ram_base_addr = Integer.parseInt(t.getText().toString().replace("0x",""),16);
                t = (EditText) v.findViewById(R.id.edit_ram_size);
                part.ram_size = Integer.parseInt(t.getText().toString().replace("0x",""),16);
                t = (EditText) v.findViewById(R.id.edit_ram_buf_offset);
                part.ram_buffer_offset = Integer.parseInt(t.getText().toString().replace("0x",""),16);
                t = (EditText) v.findViewById(R.id.edit_ram_buf_size);
                part.ram_buffer_size = Integer.parseInt(t.getText().toString().replace("0x",""),16);

                t = (EditText) v.findViewById(R.id.edit_part_name);
                part.part_name = t.getText().toString();
                t = (EditText) v.findViewById(R.id.edit_flash_nb_sector);
                part.flash_nb_sectors = Integer.parseInt(t.getText().toString());
                Spinner s = (Spinner) v.findViewById(R.id.spinner_uuencode);
                part.uuencode = s.getSelectedItemPosition() == 0;
                if(part.id == -1)
                    DatabaseManager.getInstance(getActivity()).addPart(part);
                else
                    DatabaseManager.getInstance(getActivity()).updatePart(part);

                //cleanup();
                listener.onSubmit();
            }
        });
        b = (Button) v.findViewById(R.id.cancel);
        b.setTextColor(0xFF000000);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cleanup();
                listener.onCancel();
            }
        });
        modifyBt(v);
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (OnAddInteractionListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public interface OnAddInteractionListener {
        void onCancel();
        void onSubmit();
    }
    public void modifyBt (View v){
        if (v == null)
            v = getView();
        boolean isValid = true;
        if(v == null)
            return;
        EditText t = (EditText) v.findViewById(R.id.edit_part_id);
        String hexa = t.getText().toString();
        if(!hexa.matches("0x[0-9A-F]*"))
            isValid = false;
        t = (EditText) v.findViewById(R.id.edit_flash_base_addr);
        hexa = t.getText().toString();
        if(!hexa.matches("0x[0-9A-F]*"))
            isValid = false;
        t = (EditText) v.findViewById(R.id.edit_flash_size);
        hexa = t.getText().toString();
        if(!hexa.matches("0x[0-9A-F]*"))
            isValid = false;
        t = (EditText) v.findViewById(R.id.edit_reset_vector_offset);
        hexa = t.getText().toString();
        if(!hexa.matches("0x[0-9A-F]*"))
            isValid = false;
        t = (EditText) v.findViewById(R.id.edit_ram_base_addr);
        hexa = t.getText().toString();
        if(!hexa.matches("0x[0-9A-F]*"))
            isValid = false;
        t = (EditText) v.findViewById(R.id.edit_ram_size);
        hexa = t.getText().toString();
        if(!hexa.matches("0x[0-9A-F]*"))
            isValid = false;
        t = (EditText) v.findViewById(R.id.edit_ram_buf_offset);
        hexa = t.getText().toString();
        if(!hexa.matches("0x[0-9A-F]*"))
            isValid = false;
        t = (EditText) v.findViewById(R.id.edit_ram_buf_size);
        hexa = t.getText().toString();
        if(!hexa.matches("0x[0-9A-F]*"))
            isValid = false;

        t = (EditText) v.findViewById(R.id.edit_flash_nb_sector);
        if(t.getText().toString().isEmpty())
            isValid = false;
        t = (EditText) v.findViewById(R.id.edit_part_name);
        if(t.getText().toString().isEmpty())
            isValid = false;
        Button b = (Button) v.findViewById(R.id.submit);
        if(isValid) {
            b.setTextColor(0xFF000000);
            b.setEnabled(true);
        }
        else {
            b.setTextColor(0xFFA0A0A0);
            b.setEnabled(false);
        }

    }

    public void cleanup() {
        View v = getView();
        assert v != null;
        EditText t = (EditText) v.findViewById(R.id.edit_part_id);
        t.removeTextChangedListener(watchers.get(0));
        t = (EditText) v.findViewById(R.id.edit_flash_base_addr);
        t.removeTextChangedListener(watchers.get(1));
        t = (EditText) v.findViewById(R.id.edit_flash_size);
        t.removeTextChangedListener(watchers.get(2));
        t = (EditText) v.findViewById(R.id.edit_reset_vector_offset);
        t.removeTextChangedListener(watchers.get(3));
        t = (EditText) v.findViewById(R.id.edit_ram_base_addr);
        t.removeTextChangedListener(watchers.get(4));
        t = (EditText) v.findViewById(R.id.edit_ram_size);
        t.removeTextChangedListener(watchers.get(5));
        t = (EditText) v.findViewById(R.id.edit_ram_buf_offset);
        t.removeTextChangedListener(watchers.get(6));
        t = (EditText) v.findViewById(R.id.edit_ram_buf_size);
        t.removeTextChangedListener(watchers.get(7));
        t = (EditText) v.findViewById(R.id.edit_part_name);
        t.removeTextChangedListener(watchers.get(8));
        t = (EditText) v.findViewById(R.id.edit_flash_nb_sector);
        t.removeTextChangedListener(watchers.get(9));

        watchers.clear();
    }
}
