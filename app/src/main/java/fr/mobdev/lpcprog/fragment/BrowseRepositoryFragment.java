package fr.mobdev.lpcprog.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import fr.mobdev.lpcprog.R;
import fr.mobdev.lpcprog.adapters.BinariesAdapter;
import fr.mobdev.lpcprog.listener.NetworkListener;
import fr.mobdev.lpcprog.managers.DatabaseManager;
import fr.mobdev.lpcprog.managers.NetworkManager;
import fr.mobdev.lpcprog.objects.Binary;
import fr.mobdev.lpcprog.objects.Server;

public class BrowseRepositoryFragment extends Fragment {

    private BrowseRepositoryListener browseListener;
    private List<Server> servers;

    public static BrowseRepositoryFragment newInstance(BrowseRepositoryListener browseRepositoryListener) {
        BrowseRepositoryFragment fragment = new BrowseRepositoryFragment();
        fragment.setBrowseRepositoryListener(browseRepositoryListener);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        servers = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.repository,container,false);
        RecyclerView list = v.findViewById(R.id.bin_list);
        list.setLayoutManager(new LinearLayoutManager(getActivity()));
        SwipeRefreshLayout swipeRefresh = v.findViewById(R.id.swipe_refresh);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                updateRepositories(v);
            }
        });
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateRepositories(view);
    }

    public void setBrowseRepositoryListener(BrowseRepositoryListener browseRepositoryListener) {
        browseListener = browseRepositoryListener;
    }

    public void updateRepositories(View view){
        if(view == null)
            view = getView();
        final View v = view;
        assert v != null;
        servers.clear();
        final NetworkListener listener = new NetworkListener() {
            @Override
            public void startBinaries() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        SwipeRefreshLayout swipeRefresh = v.findViewById(R.id.swipe_refresh);
                        swipeRefresh.setRefreshing(true);
                    }
                });
            }

            @Override
            public void startServer(Server server) {
            }

            @Override
            public void endServer(Server server) {
                if(!server.binaries.isEmpty())
                    servers.add(server);
            }

            @Override
            public void endBinaries() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        RecyclerView list = v.findViewById(R.id.bin_list);
                        BinariesAdapter adapter = new BinariesAdapter(servers,browseListener);
                        list.setAdapter(adapter);
                        SwipeRefreshLayout swipeRefresh = v.findViewById(R.id.swipe_refresh);
                        swipeRefresh.setRefreshing(false);

                    }
                });
            }

            @Override
            public void onError(final String error) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        final Snackbar msg = Snackbar.make(v.findViewById(R.id.swipe_refresh), error,Snackbar.LENGTH_LONG);
                        msg.show();
                        List<List<Server>> localServers = DatabaseManager.getInstance(getActivity()).getServers(getActivity());
                        servers.addAll(localServers.get(0));
                        servers.addAll(localServers.get(1));
                        servers.addAll(localServers.get(2));
                        RecyclerView list = v.findViewById(R.id.bin_list);
                        BinariesAdapter adapter = new BinariesAdapter(servers,browseListener);
                        list.setAdapter(adapter);
                        SwipeRefreshLayout swipeRefresh = v.findViewById(R.id.swipe_refresh);
                        swipeRefresh.setRefreshing(false);
                    }
                });
            }
        };
        NetworkManager.getInstance(listener).browseBinaries(getActivity());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(servers != null)
            updateRepositories(null);
    }

    public interface BrowseRepositoryListener {
        void onBinaryClick(Server server, Binary binary);
    }

}
