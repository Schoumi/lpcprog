/*
 * Copyright (C) 2016 Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.lpcprog.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import fr.mobdev.lpcprog.adapters.DeviceAdapter;
import fr.mobdev.lpcprog.R;
import fr.mobdev.lpcprog.objects.USBDevice;
import fr.mobdev.lpcprog.managers.UsbCommManager;

/*
    Fragment use to display USB device plugged through OTG port
*/
public class BrowseDeviceFragment extends Fragment {

    private List<USBDevice> devices;
    private BrowseDeviceListener listener;
    private UsbCommManager comm;

    public BrowseDeviceFragment() {

    }

    public static BrowseDeviceFragment newInstance(BrowseDeviceListener browseDeviceListener) {
        BrowseDeviceFragment fragment = new BrowseDeviceFragment();
        fragment.setBrowseDeviceListener(browseDeviceListener);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.usb_list,container,false);
        RecyclerView list = v.findViewById(R.id.device_list);
        list.setLayoutManager(new LinearLayoutManager(getActivity()));
        comm = UsbCommManager.getInstance(getActivity());

        SwipeRefreshLayout swipeRefresh = v.findViewById(R.id.swipe_refresh);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                updateDevice(v);
            }
        });
        updateDevice(v);
        return v;
    }

    public void updateDevice(View v) {
        devices = comm.getDevices();
        if(v == null)
            v = getView();
        assert v != null;
        TextView nodevice = v.findViewById(R.id.no_device);
        if(devices.size() == 0) {
            nodevice.setVisibility(View.VISIBLE);
        } else {
            nodevice.setVisibility(View.GONE);
        }
        DeviceAdapter adapter = new DeviceAdapter(devices,listener);
        RecyclerView view = v.findViewById(R.id.device_list);
        view.setAdapter(adapter);
        SwipeRefreshLayout swipeRefresh = v.findViewById(R.id.swipe_refresh);
        swipeRefresh.setRefreshing(false);
    }

    public void setBrowseDeviceListener(BrowseDeviceListener browseDeviceListener) {
        listener = browseDeviceListener;
    }

    public interface BrowseDeviceListener {
        void onDeviceClick(USBDevice device);
    }
}
