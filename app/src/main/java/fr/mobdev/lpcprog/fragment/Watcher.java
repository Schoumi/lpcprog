/*
 * Copyright (C) 2016 Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.lpcprog.fragment;

import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import fr.mobdev.lpcprog.R;

class HexaWatcher implements TextWatcher {

    private EditText v;
    private Drawable orig;
    private AddPartFragment fragment;
    HexaWatcher(EditText view,AddPartFragment fragment) {
        v = view;
        this.fragment = fragment;
        orig = v.getBackground();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        String hexa = editable.toString();
        if(!hexa.matches("0x[0-9A-F]+"))
            v.setBackgroundResource(R.drawable.apptheme_edit_text_holo_light);
        else
            v.setBackgroundDrawable(orig);
        fragment.modifyBt(null);
    }
}

class EmptyWatcher implements TextWatcher {

    private EditText v;
    private AddPartFragment fragment;
    private Drawable orig;
    EmptyWatcher(EditText view,AddPartFragment fragment) {
        v = view;
        this.fragment = fragment;
        orig = v.getBackground();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        String data = editable.toString();
        if(data.isEmpty())
            v.setBackgroundResource(R.drawable.apptheme_edit_text_holo_light);
        else
            v.setBackgroundDrawable(orig);
        fragment.modifyBt(null);
    }
}