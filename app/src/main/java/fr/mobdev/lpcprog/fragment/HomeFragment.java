/*
 * Copyright (C) 2017 Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.lpcprog.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import fr.mobdev.lpcprog.R;

/*
    Main Fragment that Handle navigation threw device or repository in the Flashing Flow
*/
public class HomeFragment extends Fragment{

    private HomeListener homeListener;

    public static HomeFragment newInstance(HomeListener listener) {
        HomeFragment fragment = new HomeFragment();
        fragment.setHomeListener(listener);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.home, container, false);
        Button bt = v.findViewById(R.id.browse_repo);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                homeListener.onBrowseRepositoryClick();
            }
        });
        bt = v.findViewById(R.id.browse_device);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                homeListener.onBrowseDeviceClick();
            }
        });
        return v;
    }

    public void setHomeListener(HomeListener homeListener) {
        this.homeListener = homeListener;
    }

    public interface HomeListener {
        void onBrowseRepositoryClick();
        void onBrowseDeviceClick();
    }

}
