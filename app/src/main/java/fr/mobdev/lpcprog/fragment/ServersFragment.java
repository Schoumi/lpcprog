/*
 * Copyright (C) 2016  Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.lpcprog.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import fr.mobdev.lpcprog.R;
import fr.mobdev.lpcprog.adapters.ServerAdapter;
import fr.mobdev.lpcprog.listener.ServerListener;
import fr.mobdev.lpcprog.managers.DatabaseManager;
import fr.mobdev.lpcprog.objects.Server;

/*
  Fragment that allow user to manage the server where he want to upload his images. Server must contains a Binaries file to work with the app
*/
public class ServersFragment extends Fragment {

    private ServerListener serverListener;

    public ServersFragment() {

    }

    public static Fragment newInstance() {
        return new ServersFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.servers,container,false);
        /* ***** Change status of the repos: active to inactive or reverse *****

        ListView serverList = (ListView) findViewById(R.id.servers_list);
        serverList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Server clickedServer = dbServers.get(position);
                DatabaseManager.getInstance(getApplicationContext()).changeServerStatus(clickedServer.id,!clickedServer.isActive);
                updateServers();
                return true;
            }
        });*/
        serverListener = new ServerListener() {
            @Override
            public void updateServerList() {
                updateServers(null);
            }
        };

        updateServers(v);
        return v;
    }

    public void updateServers(View v)
    {
        // build the view with server list in database
        List<List<Server>> dbServers = DatabaseManager.getInstance(getActivity()).getServers(getActivity());
        List<String> namesList = new ArrayList<>();
        namesList.add(getString(R.string.active_section));
        namesList.add(getString(R.string.inactive_section));
        namesList.add(getString(R.string.failed_section));
        ServerAdapter adapter = new ServerAdapter(dbServers,namesList,serverListener);
        if (v == null)
            v = getView();
        if(v!=null) {
            RecyclerView serverList = v.findViewById(R.id.servers_list);
            serverList.setLayoutManager(new LinearLayoutManager(getActivity()));
            serverList.setAdapter(adapter);
        }
    }
}
