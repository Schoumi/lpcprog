/*
 * Copyright (C) 2016 Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.lpcprog.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import fr.mobdev.lpcprog.R;
import fr.mobdev.lpcprog.adapters.PartsAdapter;
import fr.mobdev.lpcprog.managers.DatabaseManager;
import fr.mobdev.lpcprog.objects.Part;

/*
    Fragment that display list of different µC definitions
*/
public class PartsFragment extends Fragment {

    private OnEditPressListener onEditListener;

    public PartsFragment() {
    }

    public static PartsFragment newInstance() {
        return new PartsFragment();
    }

    public void setOnEditPressListener(OnEditPressListener listener){
        onEditListener = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.parts, container, false);
        RecyclerView list = (RecyclerView) v.findViewById(R.id.parts_list);
        list.setLayoutManager(new LinearLayoutManager(getActivity()));
        List<Part> parts = DatabaseManager.getInstance(getActivity()).getParts();
        PartsAdapter adapter = new PartsAdapter(parts,onEditListener);
        list.setAdapter(adapter);
        return v;
    }

    public void updateList() {
        View v = getView();
        if(v == null)
            return;
        RecyclerView list = (RecyclerView) v.findViewById(R.id.parts_list);
        List<Part> parts = DatabaseManager.getInstance(getActivity()).getParts();
        PartsAdapter adapter = new PartsAdapter(parts,onEditListener);
        list.setAdapter(adapter);
    }

    public interface OnEditPressListener{
        void onEditPress(Long part_id);
        void onDeletePress();
    }
}
