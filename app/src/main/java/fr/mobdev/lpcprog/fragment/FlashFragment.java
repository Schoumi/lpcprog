package fr.mobdev.lpcprog.fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import fr.mobdev.lpcprog.R;
import fr.mobdev.lpcprog.listener.DownloadListener;
import fr.mobdev.lpcprog.listener.FlashListener;
import fr.mobdev.lpcprog.managers.IspManager;
import fr.mobdev.lpcprog.managers.NetworkManager;
import fr.mobdev.lpcprog.managers.UsbCommManager;
import fr.mobdev.lpcprog.objects.Binary;
import fr.mobdev.lpcprog.objects.Server;
import fr.mobdev.lpcprog.objects.USBDevice;

public class FlashFragment extends Fragment{

    private USBDevice device;
    private Binary binary;
    private Server server;
    private Long part_id;
    private String uidStr;
    private String bootStr;
    private String partidStr;
    private String flash_infoStr="";
    private TextView flash_info;
    private ProgressBar progressBar;
    private FlashListener flashListener;


    public static FlashFragment newInstance(USBDevice device, Server server, Binary binary) {
        FlashFragment fragment = new FlashFragment();
        fragment.setDevice(device);
        fragment.setBinary(server, binary);
        return fragment;
    }


    @SuppressLint("SetTextI18n")
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.flash,container,false);
        TextView name = v.findViewById(R.id.name);
        name.setText(getString(R.string.dev_name)+ device.description);
        TextView vendor = v.findViewById(R.id.vendor_id);
        vendor.setText(getString(R.string.vendor_id)+String.format("%04x", device.VID));
        TextView product = v.findViewById(R.id.id);
        product.setText(getString(R.string.product_id)+String.format("%04x", device.PID));

        TextView binaryName = v.findViewById(R.id.binary_name);
        binaryName.setText(binary.name);
        TextView binaryVersion = v.findViewById(R.id.binary_version);
        binaryVersion.setText(getString(R.string.version)+" "+binary.version);
        TextView serverName = v.findViewById(R.id.server);
        serverName.setText(server.url.toString());

        flash_info = v.findViewById(R.id.flash_information);

        final Button btisp = v.findViewById(R.id.retry_isp_mode);
        btisp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getISPInfo();
                btisp.setVisibility(View.GONE);
            }
        });
        btisp.setVisibility(View.GONE);

        final Button btflash = v.findViewById(R.id.flash_device);
        btflash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flashBinary();
                btflash.setVisibility(View.GONE);
            }
        });
        btflash.setVisibility(View.GONE);

        final Button btdown = v.findViewById(R.id.retry_download);
        btdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadBinary();
                btdown.setVisibility(View.GONE);
            }
        });
        btdown.setVisibility(View.GONE);

        progressBar = v.findViewById(R.id.progress_flash);
        progressBar.setVisibility(View.GONE);

        flashListener = new FlashListener() {

            int previousStep = 0;
            int previousId = 0;
            @Override
            public void onStartFlash() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Button bt = v.findViewById(R.id.flash_device);
                        bt.setVisibility(View.GONE);
                        progressBar.setVisibility(View.VISIBLE);
                        updateProgress(0);
                    }
                });
            }

            @Override
            public void onSuccess() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Button bt = v.findViewById(R.id.flash_device);
                        bt.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        updateProgress(0);
                        flash_infoStr += getString(R.string.ok)+"\n";
                        flash_infoStr += getString(R.string.flash_succeed);
                        flash_info.setText(flash_infoStr);

                        TextView step = v.findViewById(R.id.flash_steps);
                        step.setVisibility(View.GONE);
                        previousStep = 0;
                        previousId = 0;
                    }
                });
            }

            @Override
            public void onProgress(final int step, final int endStep, final int progress, final int step_res_id) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(previousStep != step || previousId != step_res_id) {
                            if(previousStep != 0)
                                flash_infoStr += getString(R.string.ok)+"\n";
                            previousStep = step;
                            previousId = step_res_id;
                            flash_infoStr += getString(R.string.step)+" "+step+"/"+endStep+" "+getString(step_res_id)+" ";
                            flash_info.setText(flash_infoStr);

                            TextView stepV = v.findViewById(R.id.flash_steps);
                            stepV.setVisibility(View.VISIBLE);
                            stepV.setText(getString(R.string.step)+" "+step+"/"+endStep+" "+getString(step_res_id));
                        }
                        updateProgress(progress);
                    }
                });
            }

            @Override
            public void onError(final String error) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        flash_infoStr += getString(R.string.fail)+"\n";
                        flash_infoStr += error+"\n";
                        flash_info.setText(flash_infoStr);
                        Button bt = v.findViewById(R.id.flash_device);
                        bt.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        TextView step = v.findViewById(R.id.flash_steps);
                        step.setVisibility(View.GONE);
                        previousId = 0;
                        previousStep = 0;
                    }
                });
            }
        };

        return v;
    }

    private void flashBinary() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                IspManager.getInstance(getActivity()).flashBinary(server,binary,flashListener);
            }
        }).start();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getISPInfo();
    }

    private void getISPInfo(){

        View v = getView();
        assert v != null;

        final TextView step = v.findViewById(R.id.flash_steps);
        step.setVisibility(View.VISIBLE);
        step.setText(getString(R.string.retrieve_lpc_info));

        flash_infoStr += getString(R.string.retrieve_lpc_info)+" ";
        flash_info.setText(flash_infoStr);
        progressBar.setVisibility(View.VISIBLE);

        new Thread(new Runnable() {
            @Override
            public void run() {
                UsbCommManager comm = UsbCommManager.getInstance(getActivity());
                boolean conn = comm.openConnection(device);
                IspManager manager = IspManager.getInstance(getActivity());
                uidStr = "No UID Found or error";
                bootStr = "No Boot Version Found or error";
                partidStr = "No Part ID Found or error";
                boolean status = true;
                if (conn && manager.setupDevice(device, 115200)) {
                    if (!manager.synchronizeIfPossible(10000)) {
                        status = false;
                    } else {
                        updateProgress(0);
                        String[] version = manager.readBootVersion();
                        updateProgress(33);
                        String partid = manager.readPartId();
                        updateProgress(67);
                        String[] uids = manager.readUid();
                        updateProgress(100);

                        if (uids != null) {
                            uidStr = String.format("UID: %08x - %08x - %08x - %08x", Long.parseLong(uids[0]),
                                    Long.parseLong(uids[1]),
                                    Long.parseLong(uids[2]),
                                    Long.parseLong(uids[3]));
                        } else {
                            status = false;
                        }
                        if (version != null) {
                            bootStr = "Boot Version: " + version[0] + "." + version[1];
                        } else {
                            status = false;
                        }
                        if (partid != null) {
                            partidStr = String.format("Part Id: %08x", Long.parseLong(partid));
                            part_id = Long.parseLong(partid);
                            IspManager.getInstance(getActivity()).setPartId(part_id);
                        } else {
                            status = false;
                        }
                    }
                }
                final boolean statusLPCInfo = status;
                if (getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(!statusLPCInfo) {
                                View v = getView();
                                Button bt = v.findViewById(R.id.retry_isp_mode);
                                bt.setVisibility(View.VISIBLE);
                                flash_infoStr += getString(R.string.fail) + "\n";
                                flash_infoStr += getString(R.string.try_isp_again)+"\n";
                                flash_info.setText(flash_infoStr);
                            } else {
                                View v = getView();
                                Button bt = v.findViewById(R.id.retry_isp_mode);
                                bt.setVisibility(View.GONE);
                                bt = v.findViewById(R.id.flash_device);
                                bt.setVisibility(View.GONE);
                                flash_infoStr += getString(R.string.ok) + "\n";
                                flash_info.setText(flash_infoStr);
                                downloadBinary();
                            }
                            updateIDS(uidStr, partidStr, bootStr);
                            progressBar.setVisibility(View.GONE);
                            step.setVisibility(View.GONE);
                        }
                    });

                }
            }
        }).start();
    }

    private void updateProgress(final int i) {
        if(getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setProgress(i);
                }
            });
        }
    }

    private void downloadBinary() {
        final View v = getView();
        assert v != null;

        final TextView step = v.findViewById(R.id.flash_steps);
        step.setVisibility(View.VISIBLE);
        step.setText(getString(R.string.download_binary));

        progressBar.setVisibility(View.VISIBLE);

        flash_infoStr += getString(R.string.download_binary)+" ";
        flash_info.setText(flash_infoStr);

        if(binary.isDownloaded) {
            step.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);

            flash_infoStr += getString(R.string.ok)+"\n";
            flash_info.setText(flash_infoStr);
            Button bt = v.findViewById(R.id.flash_device);
            bt.setVisibility(View.VISIBLE);
            return;
        }

        DownloadListener listener = new DownloadListener() {

            @Override
            public void downloadSuccessful(Server server, Binary binary) {
                binary.isDownloading = false;
                binary.progress = 0;
                binary.isDownloaded = true;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        step.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        flash_infoStr += getString(R.string.ok)+"\n";
                        flash_info.setText(flash_infoStr);
                        Button bt = v.findViewById(R.id.flash_device);
                        bt.setVisibility(View.VISIBLE);
                    }
                });
            }

            @Override
            public void downloadError(Server server, Binary binary, final String error) {
                binary.isDownloading = false;
                binary.progress = 0;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        step.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        flash_infoStr += getString(R.string.fail)+"\n";
                        flash_infoStr += error+"\n";
                        flash_info.setText(flash_infoStr);

                        Button bt = v.findViewById(R.id.retry_download);
                        bt.setVisibility(View.VISIBLE);
                    }
                });
            }

            @Override
            public void progress(Server server, Binary binary, final int progress) {
                binary.progress = progress;
                updateProgress(progress);
            }
        };

        NetworkManager.getInstance(null).retrieveBinary(getActivity(),server,binary,listener);
    }

    private void updateIDS(String uid, String partid, String boot) {
        final View v = getView();
        assert v != null;
        TextView view = v.findViewById(R.id.uids);
        view.setText(uid);

        view = v.findViewById(R.id.part_id);
        view.setText(partid);

        view = v.findViewById(R.id.boot_version);
        view.setText(boot);
    }



    private void setBinary(Server server, Binary binary) {
        this.server = server;
        this.binary = binary;
    }

    public void setDevice(USBDevice device) {
        this.device = device;
    }
}
