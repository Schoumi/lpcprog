/*
 * Copyright (C) 2016 Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.lpcprog.objects;

/*
    Object that represent µC definition for the app
*/
public class Part {
    public long id;
    public long part_id;
    public String part_name;
    public int flash_base_addr;
    public int flash_size;
    public int flash_nb_sectors;
    public int reset_vector_offset;
    public int ram_base_addr;
    public int ram_size;
    public int ram_buffer_offset;
    public int ram_buffer_size;
    public boolean uuencode;
}
