/*
 * Copyright (C) 2016 Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.lpcprog.objects;

/*
    Object that represent a binary for the app
*/
public class Binary {
    public long id;
    public String name;
    public String filename;
    public int version;
    public String path;
    public String sha1;
    public boolean isDownloaded;
    public boolean isDownloading;
    public int progress = 0;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Binary binary = (Binary) o;

        if (version != binary.version) {
            return false;
        }
        if (!name.equals(binary.name)){
            return false;
        }
        if (!filename.equals(binary.filename)) {
            return false;
        }
        if (!path.equals(binary.path)) {
            return false;
        }
        return sha1.equals(binary.sha1);

    }
}
