/*
 * Copyright (C) 2016 Anthony Chomienne, anthony@mob-dev.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package fr.mobdev.lpcprog.activity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import fr.mobdev.lpcprog.R;
import fr.mobdev.lpcprog.dialog.ServerDialog;
import fr.mobdev.lpcprog.fragment.AddPartFragment;
import fr.mobdev.lpcprog.fragment.BrowseRepositoryFragment;
import fr.mobdev.lpcprog.fragment.FlashFragment;
import fr.mobdev.lpcprog.fragment.HomeFragment;
import fr.mobdev.lpcprog.fragment.PartsFragment;
import fr.mobdev.lpcprog.fragment.ServersFragment;
import fr.mobdev.lpcprog.fragment.BrowseDeviceFragment;
import fr.mobdev.lpcprog.listener.ServerListener;
import fr.mobdev.lpcprog.objects.Binary;
import fr.mobdev.lpcprog.objects.Server;
import fr.mobdev.lpcprog.objects.USBDevice;

/*
    The only activity of the app. It Handle all change of fragment depending on user actions
    the Menu change with fragment
*/

public class MainActivity extends AppCompatActivity implements  NavigationView.OnNavigationItemSelectedListener,
                                                                AddPartFragment.OnAddInteractionListener
{

    BrowseDeviceFragment.BrowseDeviceListener deviceListener;
    BrowseRepositoryFragment.BrowseRepositoryListener repositoryListener;
    Binary binary;
    Server server;
    USBDevice device;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        final FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        Fragment frag = HomeFragment.newInstance(new HomeFragment.HomeListener() {
            @Override
            public void onBrowseRepositoryClick() {
                FragmentTransaction transaction = manager.beginTransaction();
                Fragment frag = BrowseRepositoryFragment.newInstance(repositoryListener);
                transaction.addToBackStack(null);
                transaction.replace(R.id.fragment_container, frag);
                transaction.commit();
                updateMenu(null);
            }

            @Override
            public void onBrowseDeviceClick() {
                FragmentTransaction transaction = manager.beginTransaction();
                Fragment frag = BrowseDeviceFragment.newInstance(deviceListener);
                transaction.addToBackStack(null);
                transaction.replace(R.id.fragment_container, frag);
                transaction.commit();
                updateMenu(null);
            }
        });
        transaction.replace(R.id.fragment_container, frag);
        transaction.commit();

        IntentFilter filter = new IntentFilter("android.hardware.usb.action.USB_DEVICE_ATTACHED");
        filter.addAction("android.hardware.usb.action.USB_DEVICE_DETACHED");
        filter.addAction("com.ftdi.j2xx");
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Fragment fragment = manager.findFragmentById(R.id.fragment_container);
                if(fragment instanceof BrowseDeviceFragment) {
                    ((BrowseDeviceFragment)fragment).updateDevice(null);
                }
                else if(fragment instanceof FlashFragment){
                    if(intent.getAction().equals("android.hardware.usb.action.USB_DEVICE_DETACHED")) {
                        //TODO Add dialog deconnected device
                        binary = null;
                        server = null;
                        device = null;
                        manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }
                }
            }
        },filter);

        deviceListener = new BrowseDeviceFragment.BrowseDeviceListener() {
            @Override
            public void onDeviceClick(USBDevice device) {
                MainActivity.this.device = device;
                if(binary != null) {
                    FragmentManager manager = getFragmentManager();
                    FragmentTransaction transaction = manager.beginTransaction();
                    Fragment frag = FlashFragment.newInstance(device,server, binary);
                    transaction.replace(R.id.fragment_container, frag);
                    transaction.addToBackStack(null);
                    transaction.commit();
                    updateMenu(null);
                } else {
                    FragmentManager manager = getFragmentManager();
                    FragmentTransaction transaction = manager.beginTransaction();
                    Fragment frag = BrowseRepositoryFragment.newInstance(repositoryListener);
                    transaction.replace(R.id.fragment_container, frag);
                    transaction.addToBackStack(null);
                    transaction.commit();
                    updateMenu(null);
                }
            }
        };

        repositoryListener = new BrowseRepositoryFragment.BrowseRepositoryListener() {
            @Override
            public void onBinaryClick(Server server, Binary binary) {
                MainActivity.this.server = server;
                MainActivity.this.binary = binary;
                if(device != null) {
                    FragmentManager manager = getFragmentManager();
                    FragmentTransaction transaction = manager.beginTransaction();
                    Fragment frag = FlashFragment.newInstance(device,server, binary);
                    transaction.replace(R.id.fragment_container, frag);
                    transaction.addToBackStack(null);
                    transaction.commit();
                    updateMenu(null);
                } else {
                    FragmentManager manager = getFragmentManager();
                    FragmentTransaction transaction = manager.beginTransaction();
                    Fragment frag = BrowseDeviceFragment.newInstance(deviceListener);
                    transaction.replace(R.id.fragment_container, frag);
                    transaction.addToBackStack(null);
                    transaction.commit();
                    updateMenu(null);
                }

            }
        };

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            FragmentManager manager = getFragmentManager();
            Fragment frag = manager.findFragmentById(R.id.fragment_container);
            if(frag instanceof BrowseDeviceFragment) {
                device = null;
            }
            else if(frag instanceof BrowseRepositoryFragment) {
                binary = null;
                server = null;
            }
            updateMenu(null);
        }
    }

    private void updateMenu(Fragment frag) {
        Toolbar bar = (Toolbar) findViewById(R.id.toolbar);
        Menu menu = bar.getMenu();
        MenuItem add = menu.findItem(R.id.action_add_new);
        if(frag == null) {
            FragmentManager manager = getFragmentManager();
            frag = manager.findFragmentById(R.id.fragment_container);
        }
        if(frag instanceof PartsFragment || frag instanceof ServersFragment) {
            add.setVisible(true);
            if(frag instanceof PartsFragment)
                add.setTitle(R.string.add_parts);
            else
                add.setTitle(R.string.add_server);
        } else {
            add.setVisible(false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        updateMenu(null);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        FragmentManager manager = getFragmentManager();
        final Fragment frag = manager.findFragmentById(R.id.fragment_container);
        if (id == R.id.action_add_new) {

            if(frag instanceof ServersFragment) {
                ServerDialog serverDialog = new ServerDialog();
                serverDialog.setServerListener(new ServerListener() {
                    @Override
                    public void updateServerList() {
                        ((ServersFragment) frag).updateServers(null);
                    }
                });
                serverDialog.show(getSupportFragmentManager(),"Server Dialog");
            } else if(frag instanceof PartsFragment) {
                FragmentTransaction transaction = manager.beginTransaction();
                Fragment fragment = AddPartFragment.newInstance(-1);
                transaction.replace(R.id.fragment_container, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
                updateMenu(fragment);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        Fragment frag = null;
        final FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        final Fragment old = manager.findFragmentById(R.id.fragment_container);
        if(id == R.id.nav_home) {
            manager.popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
            binary = null;
            server = null;
            device = null;
            updateMenu(null);
        } else if (id == R.id.nav_servers) {
            if(!(old instanceof ServersFragment)) {
                frag = ServersFragment.newInstance();
                transaction.replace(R.id.fragment_container, frag);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        } else if (id == R.id.nav_parts) {
                if(!(old instanceof PartsFragment)) {
                    frag = PartsFragment.newInstance();
                    final PartsFragment fragmentParts = (PartsFragment) frag;
                    fragmentParts.setOnEditPressListener(new PartsFragment.OnEditPressListener() {

                        @Override
                        public void onEditPress(Long part_id) {
                            Fragment fragment = AddPartFragment.newInstance(part_id);
                            FragmentTransaction transaction = manager.beginTransaction();
                            transaction.replace(R.id.fragment_container, fragment);
                            transaction.addToBackStack(null);
                            transaction.commit();
                            fragmentParts.updateList();
                            updateMenu(fragment);

                        }

                        @Override
                        public void onDeletePress() {
                            fragmentParts.updateList();
                        }
                    });
                    transaction.replace(R.id.fragment_container, frag);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        updateMenu(frag);
        return true;
    }




    @Override
    public void onCancel() {
        onBackPressed();
    }

    @Override
    public void onSubmit() {
        onBackPressed();
        FragmentManager manager = getFragmentManager();
        Fragment frag = manager.findFragmentById(R.id.fragment_container);
        if(frag instanceof PartsFragment) {
            ((PartsFragment)frag).updateList();
            updateMenu(frag);
        }
    }
}
