# Privacy Policy (Android)

The LPCProg Android application does not collect any personal information.

The application requires the following permissions:

    ACCESS_NETWORK_STATE: This permission allows the application to know whether you are connected to the Internet before making any request.
    INTERNET: This permission is required so that the application can download binaries to flash on user hardware.

Check privacy policy of binary server provider. We don't own any binary server ourself.

The application does not contain any tracker, you can check it on its report: [https://reports.exodus-privacy.eu.org/en/reports/fr.mobdev.lpcprog/latest/](https://reports.exodus-privacy.eu.org/en/reports/fr.mobdev.lpcprog/latest/)

