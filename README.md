# Lpcprog

[![build status](https://git.mob-dev.fr/Schoumi/lpcprog/badges/master/build.svg)](https://git.mob-dev.fr/Schoumi/lpcprog/commits/master)

Lpcprog is an Android application.
Lpcprog let you find and flash your NXP LPC Microcontroller.

  * Find your Microcontroller plugged in your Android Device(OTG)
  * Flash your software found on your device or on a webserver
  * HTTP & HTTPS support

Download it on the playstore: https://play.google.com/store/apps/details?id=fr.mobdev.lpcprog

<br/>
<noscript><a href="https://liberapay.com/Schoumi/donate"><img src="https://liberapay.com/assets/widgets/donate.svg"></a></noscript>